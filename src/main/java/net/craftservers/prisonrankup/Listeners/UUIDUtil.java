package net.craftservers.prisonrankup.Listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.HashMap;
import java.util.UUID;

public class UUIDUtil implements Listener{

    public static HashMap<String, String> uuidstorage = new HashMap<>();

    @EventHandler(priority = EventPriority.LOWEST)
    public void onJoin(PlayerJoinEvent event) {
        uuidstorage.put(event.getPlayer().getName(), event.getPlayer().getUniqueId().toString().replaceAll("-", ""));
    }

    @EventHandler
    public void onLeave(PlayerQuitEvent event) {
        uuidstorage.remove(event.getPlayer().getName());
    }

    public static String formatUUID(UUID uuid) {
        return uuid.toString().replaceAll("-", "");
    }

}
