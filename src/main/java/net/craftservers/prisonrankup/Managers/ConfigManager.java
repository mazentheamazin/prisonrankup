package net.craftservers.prisonrankup.Managers;

import net.craftservers.prisonrankup.PR;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;

public class ConfigManager {

    private FileConfiguration customConfig = null;
    private File customConfigFile = null;

    public FileConfiguration getUserConfig() {
        if (customConfig == null) {
            reloadUserConfig();
        }
        return customConfig;
    }

    public void reloadUserConfig() {
        if (customConfigFile == null) {
            customConfigFile = new File(PR.getPlugin().getDataFolder(), "data.yml");
        }
        customConfig = YamlConfiguration.loadConfiguration(customConfigFile);
    }

    public void saveUserConfig() {
        if (customConfig == null || customConfigFile == null) {
            reloadUserConfig();
        }
        try {
            getUserConfig().save(customConfigFile);
        } catch (IOException ex) {
            PR.getPlugin().getLogger().log(Level.SEVERE, "Could not save config to " + customConfigFile, ex);
        }
    }

    public ConfigurationSection getUserSection() {
        if(!getUserConfig().contains("users")) {
            getUserConfig().createSection("users");
            saveUserConfig();
            reloadUserConfig();
        }
        return getUserConfig().getConfigurationSection("users");
    }

    public static FileConfiguration getConfig() {
        return PR.getPlugin().getConfig();
    }

    public static void reloadConfig() {
        PR.getPlugin().reloadConfig();
        PR.getPlugin().saveConfig();
    }

    public static void saveConfig() {
        PR.getPlugin().saveConfig();
        PR.getPlugin().reloadConfig();
    }

}
