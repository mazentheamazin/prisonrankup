package net.craftservers.prisonrankup.Models;

import net.craftservers.prisonrankup.Managers.ConfigManager;
import net.craftservers.prisonrankup.Managers.Manager;
import net.craftservers.prisonrankup.Utils.Lang;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashMap;

public class CommandHandler implements CommandExecutor{

    static HashMap<String, SubCommand> data = new HashMap<>();

    public static void registerSubCommand(String subCommand, SubCommand subCmd) {
        data.put(subCommand, subCmd);
    }

    public static SubCommand getSubCommand(String subCommand) {
        return data.get(subCommand);
    }

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if(cmd.getName().equalsIgnoreCase("rankup")) {
            if(sender instanceof Player) {
                if(args.length >= 1) {
                    if(data.containsKey(args[0])) {
                        StringBuilder sb = new StringBuilder("");

                        for(int i = 1; i < args.length; i++) {
                            sb.append(args[i]);
                            sb.append("//");
                        }

                        String[] newargs = sb.toString().split("//");
                        getSubCommand(args[0]).onCommand((Player) sender, cmd, label, newargs);
                    }else{
                        Lang.description(sender);
                    }
                }else{
                    getSubCommand("rankup").onCommand((Player) sender, cmd, label, args);
                }
            }else{
                if(args.length >= 1) {
                    if(data.containsKey(args[0])) {
                        StringBuilder sb = new StringBuilder("");

                        for(int i = 1; i < args.length; i++) {
                            sb.append(args[i]);
                            sb.append("//");
                        }

                        String[] newargs = sb.toString().split("//");
                        getSubCommand(args[0]).onCommand(sender, cmd, label, newargs);
                    }
                }else{
                    Lang.description(sender);
                }
            }
        }else if(cmd.getName().equalsIgnoreCase("ranks")) {
            if(sender instanceof Player) {
                ConfigManager.reloadConfig();
                sender.sendMessage(ChatColor.GOLD + "All ranks:");

                for(String s : Manager.getRankManager().getAllRanks()) {
                    Rank r = new Rank(s);

                    String formatString = ConfigManager.getConfig().getString("ranks-format").replaceAll("%rank%", r.getName()).replaceAll("%price%", r.getPriceString());
                    formatString = Lang.translateToColor(formatString);
                    sender.sendMessage(formatString);
                }
            }else{
                sender.sendMessage("Only players can run this command!");
            }
        }
        return false;
    }

}
