package net.craftservers.prisonrankup.Models;

import net.craftservers.prisonrankup.Managers.ConfigManager;
import net.craftservers.prisonrankup.Managers.Manager;
import net.craftservers.prisonrankup.Managers.RankManager;
import net.craftservers.prisonrankup.PR;

import java.util.ArrayList;
import java.util.List;

public class Rank {

    private String name;
    private double price;
    private String priceString;
    private List<String> playersInRank;
    private RankManager rm = Manager.getRankManager();

    public Rank(String name) {
        String[] data = rm.getDataByName(name).split(":");

        if(data.length == 1) {
            throw new IllegalArgumentException("The rank provided '" + name + "' does not exist, please make sure data.yml is updated to the contents of config.yml!");
        }

        PR.debug("Recieved data: " + data[0]);
        this.name = data[0];
        price = Double.parseDouble(data[1]);
        priceString = data[1];
        playersInRank = new ArrayList<>();
    }

    public Rank(String name, int location) {
        String[] data = ConfigManager.getConfig().getStringList("groups").get(location).split(":");
        PR.debug("Recieved data: " + data[0] + ":" + data[1] + ", " + location);
        this.name = data[0];
        PR.debug("Rank name updated");
        price = Double.parseDouble(data[1]);
        priceString = data[1];
        playersInRank = new ArrayList<>();
    }

    public String getName() {return name;}

    public double getPrice() {return price;}

    public String getPriceString() {return priceString;}

    public List<String> getPlayersInRank() {
        final List<String> plersinRank = new ArrayList<>();
        Runnable rn = new Runnable() {
            @Override
            public void run() {
                for(String id : playersInRank) {
                    plersinRank.add(id);
                }
            }
        };
        Thread thread = new Thread(rn);
        thread.run();
        return plersinRank;
    }

    public String getAllPlayers() {
        StringBuilder sb = new StringBuilder("");
        for(int i = 0; i < getPlayersInRank().size(); i++) {
            sb.append(getPlayersInRank().get(i));
            sb.append(", ");
        }
        return sb.toString();
    }

    @Override
    public String toString() {
        return "{Rank, name='" + name + "', price='" + price + "'}";
    }

    public List<String> toArray() {
        List<String> ls = new ArrayList<>();
        ls.add("name: " + name);
        ls.add("price: " + price);
        StringBuilder sb = new StringBuilder("");
        for(String s : getPlayersInRank()) {
            sb.append(playersInRank);
            sb.append(", ");
        }
        ls.add("players: " + sb.toString());
        return ls;
    }

    public int getLocation() {
        return Manager.getRankManager().getLocation(getName());
    }


}
