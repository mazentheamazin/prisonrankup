package net.craftservers.prisonrankup;

import net.craftservers.prisonrankup.Listeners.*;
import net.craftservers.prisonrankup.Managers.Manager;
import net.craftservers.prisonrankup.Models.CommandHandler;
import net.craftservers.prisonrankup.SubCommands.*;
import net.craftservers.prisonrankup.Utils.GAEUtil;
import net.craftservers.prisonrankup.Utils.UUIDFetcher;
import net.craftservers.prisonrankup.Utils.Updater;
import net.milkbowl.vault.chat.Chat;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PR extends JavaPlugin{

    private static Permission permission = null;
    private static Economy economy = null;
    private static Chat chat = null;
    private static PR pr;
    private static Logger logger;
    public static boolean update = false;
    public static String name = "";
    public static Updater.ReleaseType type = null;
    public static String version = "";
    public static String link = "";
    public static final int PROJECT_ID = 72819;

    @Override
    public void onEnable() {
        //Define some variables
        pr = this;
        logger = getLogger();

        //Setup Vault
        try{
            setupChat();
            setupEconomy();
            setupPermissions();
        }catch(NullPointerException e) {logger.log(Level.SEVERE, "Unable to loadup vault, disabling PrisonRankup"); getServer().getPluginManager().disablePlugin(this);}

        //Register Commands and Listeners
        registerCommands();
        registerListeners();

        //Startup migration process if needed
        try{
            this.migrate();
        }catch(IOException e) {logger.log(Level.SEVERE, "Unable to migrate to new config for v2.0, caused by " + e.getCause());}
        catch(InvalidConfigurationException e) {logger.log(Level.SEVERE, "~config.yml is not defined properly, caused by " + e.getCause());}
        this.saveDefaultConfig();

        //Create data.yml if does not exist
        File fl = new File(getDataFolder(), "data.yml");
        if(!fl.exists()) {
            try{
                boolean ignored = fl.createNewFile();
                FileConfiguration fg = YamlConfiguration.loadConfiguration(fl);
                fg.createSection("users");
                fg.save(fl);
                Manager.getConfigManager().reloadUserConfig();
            }catch(IOException e) {logger.log(Level.SEVERE, "Unable to create data.yml, caused by " + e.getCause());}
        }

        //Setup updater
        Updater updater = new Updater(this, PROJECT_ID, this.getFile(), Updater.UpdateType.NO_DOWNLOAD, false);
        update = updater.getResult() == Updater.UpdateResult.UPDATE_AVAILABLE;
        name = updater.getLatestName();
        version = updater.getLatestGameVersion();
        type = updater.getLatestType();
        link = updater.getLatestFileLink();

        //Reload the user configuration file
        Manager.getConfigManager().reloadUserConfig();

        //Update the configuration file for any updates
        updateConfig();

        // Little change made for @psycowithespn's mistake on approving the plugin <3 ~Mazen
    }

    @Override
    public void onDisable() {
        pr = null;
        logger = null;
    }

    private void registerListeners() {
        PluginManager pm = getServer().getPluginManager();
        pm.registerEvents(new GAEUtil(), this);
        pm.registerEvents(new ConfigUpdater(), this);
        pm.registerEvents(new JoinHandler(), this);
        pm.registerEvents(new SignHandler(), this);
        pm.registerEvents(new TimeHandler(), this);
        pm.registerEvents(new UUIDUtil(), this);
    }

    private void migrate() throws IOException, InvalidConfigurationException{
        if(getConfig().get("users") != null) {
            log("Migrating all players");
            File fl = new File(getDataFolder(), "config.yml");
            boolean fg = fl.renameTo(new File(getDataFolder(), "~config.yml"));
            File fle = new File(getDataFolder(), "~config.yml");
            FileConfiguration config = YamlConfiguration.loadConfiguration(fle);
            HashMap<String, String> tmp = new HashMap<>();

            for(String s: config.getConfigurationSection("users").getKeys(false)) {
                tmp.put(UUIDFetcher.getUUIDFromName(s).replaceAll("-", ""), config.getString("users." + s + ".group"));
                log("Migrated player: " + s);
            }

            File dat = new File(getDataFolder(), "data.yml");
            dat.createNewFile();
            FileConfiguration data = YamlConfiguration.loadConfiguration(dat);
            data.createSection("users");

            for(Map.Entry<String, String> mp : tmp.entrySet()) {
                data.set("users." + mp.getKey() + ".group", mp.getValue());
            }

            data.save(dat);
            data.load(dat);
        }
    }

    private boolean setupPermissions() {
        RegisteredServiceProvider<Permission> permissionProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.permission.Permission.class);
        if (permissionProvider != null) {
            permission = permissionProvider.getProvider();
        }
        return (permission != null);
    }

    private boolean setupChat() {
        RegisteredServiceProvider<Chat> chatProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.chat.Chat.class);
        if (chatProvider != null) {
            chat = chatProvider.getProvider();
        }

        return (chat != null);
    }

    private boolean setupEconomy() {
        RegisteredServiceProvider<Economy> economyProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class);
        if (economyProvider != null) {
            economy = economyProvider.getProvider();
        }

        return (economy != null);
    }

    public static Permission getPermission() {return permission;}

    public static Chat getChat() {return chat;}

    public static Economy getEconomy() {return economy;}

    public static PR getPlugin() {return pr;}

    public static void debug(String message) {
       if(getPlugin().getConfig().getBoolean("debug")) {
           logger.info("DEBUG: " + message);
       }
    }

    public static void log(String message) {
        logger.info(message);
    }

    private void registerCommands() {
        new Get();
        new Rankup();
        new net.craftservers.prisonrankup.SubCommands.Set();
        new Stats();
        new Reload();
        new Create();
        new Update();
        getCommand("rankup").setExecutor(new CommandHandler());
        getCommand("ranks").setExecutor(new CommandHandler());
    }

    public File getFile() {
        return super.getFile();
    }

    private void updateConfig() {
        if(getConfig().getString("ranks-format") == null)
            getConfig().createSection("ranks-format");
            getConfig().set("ranks-format", YamlConfiguration.loadConfiguration(getResource("config.yml")).getString("ranks-format"));

        if(getConfig().getString("not-enough-money") == null)
            getConfig().createSection("not-eough-money");
            getConfig().set("not-enough-money", YamlConfiguration.loadConfiguration(getResource("config.yml")).getString("not-enough-money"));

        getLogger().info("Updated configuration files for any updates");
    }

}
