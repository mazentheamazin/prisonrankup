package net.craftservers.prisonrankup;

import net.craftservers.prisonrankup.Managers.Manager;
import net.craftservers.prisonrankup.Models.PRPlayer;
import net.craftservers.prisonrankup.Models.Rank;
import org.bukkit.entity.Player;

public class PrisonRankup {

    public static Rank getNextRank(String playerName) {
        return new PRPlayer(playerName).getNextRank();
    }

    public static Rank getNextRank(Player player) {
        return new PRPlayer(player.getName()).getNextRank();
    }

    public static void rankup(Player player) {
        player.performCommand("rankup");
    }

    public static Rank getRank(String rank) {
        return Manager.getRankManager().getAllRanks().contains(rank) ? new Rank(rank) : null;
    }

    public static PRPlayer getPlayer(String name) {
        return new PRPlayer(name);
    }

}
