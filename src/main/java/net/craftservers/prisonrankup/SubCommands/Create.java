package net.craftservers.prisonrankup.SubCommands;

import net.craftservers.prisonrankup.Managers.Manager;
import net.craftservers.prisonrankup.Managers.RankManager;
import net.craftservers.prisonrankup.Models.SubCommand;
import net.craftservers.prisonrankup.Utils.Lang;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Create extends SubCommand{

    public Create() {
        super("create");
    }

    @Override
    public void onCommand(Player player, Command cmd, String label, String[] args) {
        onCommand((CommandSender) player, cmd, label, args);
    }

    @Override
    public void onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if(!sender.hasPermission("prisonrankup.create")) {
            sender.sendMessage(Lang.noPermissions);
        }else if(args.length >= 2) {
            String group = args[0];
            double price = 0.0;
            RankManager rm = Manager.getRankManager();

            try{
                price = Double.parseDouble(args[1]);
            }catch(NumberFormatException e) {sender.sendMessage(ChatColor.DARK_RED + "Incorrect syntax! /rankup create <group> <price> [index]"); return;}

            int index = 69696969;
            try{
                index = Integer.parseInt(args[2]);
            }catch(NumberFormatException ignored) {sender.sendMessage(ChatColor.DARK_RED + "Incorrect syntax! /rankup create <group> <price> [index]"); return;}
            catch(IndexOutOfBoundsException ignored) {}

            if(index != 69696969) {
                rm.createRank(group, price, index);
            }else{
                rm.createRank(group, price);
            }

            sender.sendMessage(Lang.prefix + ChatColor.GOLD + "Created rank successfully!");
        }else{
            sender.sendMessage(ChatColor.DARK_RED + "Incorrect syntax! /rankup create <group> <price> [index]");
        }
    }
}
