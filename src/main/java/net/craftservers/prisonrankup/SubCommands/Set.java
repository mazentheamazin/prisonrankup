package net.craftservers.prisonrankup.SubCommands;

import net.craftservers.prisonrankup.Managers.Manager;
import net.craftservers.prisonrankup.Models.PRPlayer;
import net.craftservers.prisonrankup.Models.Rank;
import net.craftservers.prisonrankup.Models.SubCommand;
import net.craftservers.prisonrankup.Utils.Lang;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Set extends SubCommand{

    public Set() {
        super("set");
    }

    @Override
    public void onCommand(Player player, Command cmd, String label, String[] args) {
        this.onCommand((CommandSender) player, cmd, label, args);
    }

    @Override
    public void onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if(!sender.hasPermission("prisonrankup.set")) {
            sender.sendMessage(Lang.noPermissions);
            return;
        }

        if(args.length >= 2) {
            if(!Manager.getRankManager().getAllRanks().contains(args[1])) {
                sender.sendMessage(Lang.error("Group " + args[1] + " does not exist!"));
                return;
            }

            PRPlayer player = new PRPlayer(args[0]);

            if(player.getPlayer() == null) {
                sender.sendMessage(Lang.error("Player is not online!"));
                return;
            }

            Rank newrank = new Rank(args[1]);
            player.setRank(newrank);

            sender.sendMessage(Lang.prefix + ChatColor.GOLD + "Successfully set " + player.getName()  + " to " + newrank.getName() + "!");
        }
    }
}
