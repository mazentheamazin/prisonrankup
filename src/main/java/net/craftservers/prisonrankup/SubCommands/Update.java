package net.craftservers.prisonrankup.SubCommands;

import net.craftservers.prisonrankup.Models.SubCommand;
import net.craftservers.prisonrankup.PR;
import net.craftservers.prisonrankup.Utils.Lang;
import net.craftservers.prisonrankup.Utils.Updater;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Update extends SubCommand{

    public Update() {
        super("update");
    }

    @Override
    public void onCommand(Player player, Command cmd, String label, String[] args) {
        this.onCommand((CommandSender) player, cmd, label, args);
    }

    @Override
    public void onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if(sender.hasPermission("prisonrankup.update")) {
            Updater updater = new Updater(PR.getPlugin(), PR.PROJECT_ID, PR.getPlugin().getFile(), Updater.UpdateType.NO_VERSION_CHECK, true);

            if(updater.getResult().equals(Updater.UpdateResult.SUCCESS)) {
                sender.sendMessage(ChatColor.GOLD + "Updated PrisonRankup to: " + updater.getLatestGameVersion());
                sender.sendMessage(ChatColor.GOLD + "Please restart for all changes to take effect!");
            }
        }else{
            sender.sendMessage(Lang.noPermissions);
        }
    }
}
