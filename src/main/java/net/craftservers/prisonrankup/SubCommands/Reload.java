package net.craftservers.prisonrankup.SubCommands;

import net.craftservers.prisonrankup.Managers.ConfigManager;
import net.craftservers.prisonrankup.Managers.Manager;
import net.craftservers.prisonrankup.Models.SubCommand;
import net.craftservers.prisonrankup.Utils.Lang;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Reload extends SubCommand{

    public Reload() {
        super("reload");
    }


    @Override
    public void onCommand(Player player, Command cmd, String label, String[] args) {
        this.onCommand((CommandSender) player, cmd, label, args);
    }

    @Override
    public void onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if(sender.hasPermission("prisonrankup.reload")) {
            sender.sendMessage(Lang.prefix + ChatColor.GOLD + "Reloaded configuration file!");

            ConfigManager.reloadConfig();
            Manager.getConfigManager().reloadUserConfig();
        }
    }
}
