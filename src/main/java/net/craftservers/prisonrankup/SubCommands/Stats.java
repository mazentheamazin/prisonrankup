package net.craftservers.prisonrankup.SubCommands;

import net.craftservers.prisonrankup.Managers.ConfigManager;
import net.craftservers.prisonrankup.Managers.Manager;
import net.craftservers.prisonrankup.Models.Rank;
import net.craftservers.prisonrankup.Models.SubCommand;
import net.craftservers.prisonrankup.Utils.Lang;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Stats extends SubCommand{

    public Stats() {
        super("stats");
    }

    @Override
    public void onCommand(Player player, Command cmd, String label, String[] args) {
        this.onCommand((CommandSender) player, cmd, label, args);
    }

    @Override
    public void onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if(!sender.hasPermission("prisonrankup.stats")) {
            sender.sendMessage(Lang.noPermissions);
            return;
        }
        if(args.length <= 1) {
            if(!Manager.getRankManager().getAllRanks().contains(args[0])) {
                sender.sendMessage(Lang.error("Rank does not exist!"));
                return;
            }

            Rank rank = new Rank(args[0]);
            String prefix = ConfigManager.getConfig().getString("Prefix");

            sender.sendMessage(ChatColor.GREEN + "----------" + ChatColor.translateAlternateColorCodes('&', prefix.replaceAll(" ", "") + "---------"));
            sender.sendMessage(ChatColor.AQUA + rank.getName() + "'s Stats:");
            sender.sendMessage(ChatColor.AQUA + "Price: " + rank.getPriceString());
            //sender.sendMessage(ChatColor.AQUA + "Amount of players in rank: " + rank.getPlayersInRank().size());
            if(!rank.getAllPlayers().equals(" ")) {
                sender.sendMessage(ChatColor.AQUA + "Current players in rank: " + rank.getAllPlayers());
            }
            sender.sendMessage(ChatColor.GREEN + "----------" + ChatColor.translateAlternateColorCodes('&', prefix.replaceAll(" ", "") + "---------"));
        }
    }
}
